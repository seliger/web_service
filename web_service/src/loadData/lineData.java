package loadData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import database.tables.Contractor;
import database.tables.Group;
import database.tables.Nomenclature;
import database.tables.Unit;
import main.kernel;

/**
 * Строка данных
 * @author den
 *
 */
@XmlType(name = "row", namespace = "http://main/")
@XmlAccessorType(XmlAccessType.NONE)
public class lineData {
	@XmlElement(namespace = "http://main/")
	public String inn;
	@XmlElement(namespace = "http://main/")
	public String numberOrder;
	@XmlElement(namespace = "http://main/")
	public String code;
	@XmlElement(namespace = "http://main/")
	public String subCode;
	@XmlElement(namespace = "http://main/")
	public String nameNom;
	@XmlElement(namespace = "http://main/")
	public String codeNom;
	@XmlElement(namespace = "http://main/")
	public String codeOKEI;
	@XmlElement(namespace = "http://main/")
	public double quantity;
	@XmlElement(namespace = "http://main/")
	public long cost;
	@XmlElement(namespace = "http://main/")
	public String description;
	@XmlElement(namespace = "http://main/")
	public String codeOB;
	@XmlElement(namespace = "http://main/")
	public boolean substandard;
	@XmlElement(namespace = "http://main/")
	public String shelfLife;
	@XmlElement(namespace = "http://main/")
	public boolean urgentSale;
	@XmlElement(namespace = "http://main/")
	public String comment;
	
	public Contractor getContractor() {
		Contractor retValue = kernel.getInstance().loadContByINN(inn);
		
		return retValue;
	}
	
	public Nomenclature getNomenclature() {
		Nomenclature retValue = kernel.getInstance().loadNomByCode(codeNom);
		// Если номенклатура не найдена, создадим
		if (retValue == null)
			retValue = new Nomenclature();
		
		retValue.setName(nameNom);
		retValue.setCode(code);
		retValue.setSubcode(subCode);
		retValue.setExt_id(codeNom);
		retValue.setBaseUnit(getMeasurement());
		
		return retValue;
	}
	
	public Unit getMeasurement() {
		Unit retValue = kernel.getInstance().loadUnitByCode(codeOKEI);
		
		return retValue;
	}
	
	public Group getGroup() {
		Group retValue = kernel.getInstance().loadGroupByCode(subCode);
		
		if (retValue == null)
			retValue = kernel.getInstance().loadGroupByCode(code);
		
		return retValue;
	}

	public Date getShelfLifeDate() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		return sdf.parse(shelfLife);
	}
}

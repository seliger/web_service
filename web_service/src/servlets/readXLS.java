package servlets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;

import loadData.lineData;
import main.kernel;
import main.loadData;

/**
 * Servlet implementation class readXLS
 */
@WebServlet("/readXLS")
public class readXLS extends HttpServlet {
	private Logger log = Logger.getLogger("readXLS");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public readXLS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		byte[] imageByte = Base64.getDecoder().decode(request.getParameter("file"));

		InputStream fis = new ByteArrayInputStream(imageByte);

		XSSFWorkbook wbXLSX = null;
		List<String> listColumns = new ArrayList<String>();
		try {
			wbXLSX = new XSSFWorkbook(fis);
			XSSFSheet curSheet = wbXLSX.getSheetAt(0);
			Iterator<Row> it = curSheet.iterator();
			Row curRow = it.next();
			Cell curCell = curRow.getCell(0);
			
			// Если в первой строке в первом столбце ИНН, то начинаем обработку
			if (curCell.getStringCellValue().equals("ИНН")) {
				List<lineData> listData = new ArrayList<lineData>();
				while (it.hasNext()) {
	                curRow = it.next();
	                lineData tmp = new lineData();
	                // Обрабатываем первые 15 столбцов
	                for (int i = 0; i < 15; i++) {
	                	curCell = curRow.getCell(i);
						switch (i) {
						case 0:
							tmp.inn = curCell.getStringCellValue();
							break;
						case 1:
							tmp.numberOrder = curCell.getStringCellValue();
							break;
						case 2:
							tmp.code = curCell.getStringCellValue();
							break;
						case 3:
							tmp.subCode = curCell.getStringCellValue();
							break;
						case 4:
							tmp.nameNom = curCell.getStringCellValue();
							break;
						case 5:
							tmp.codeNom = curCell.getStringCellValue();
							break;
						case 6:
							tmp.codeOKEI = curCell.getStringCellValue();
							break;
						case 7:
							tmp.quantity = Integer.parseInt(curCell.getStringCellValue());
							break;
						case 8:
							tmp.cost = (int) Double.parseDouble(curCell.getStringCellValue()) * 100;
							break;
						case 9:
							tmp.description = curCell.getStringCellValue();
							break;
						case 10:
							tmp.codeOB = curCell.getStringCellValue();
							break;
						case 11:
							tmp.substandard = curCell.getStringCellValue().equals("Истина");
							break;
						case 12:
							tmp.shelfLife = curCell.getStringCellValue();
							break;
						case 13:
							tmp.urgentSale = curCell.getStringCellValue().equals("Истина");
							break;
						case 14:
							tmp.comment = curCell.getStringCellValue();
							break;
						}
						listData.add(tmp);
	                }
				}
				new loadData().Save(listData);
			}
			
			it = null;
			wbXLSX.close();
			curSheet = null;
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			for (StackTraceElement elem : e.getStackTrace()) {
				log.error("at " + elem);
			}
		}
		if (fis != null) {
			try {
				fis.close();
			} catch (IOException e) {
				log.error(e.getMessage());
			}
			fis = null;
		}
		listColumns.clear();
		listColumns = null;
		wbXLSX = null;
	}

}

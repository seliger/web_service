package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import database.tables.Group;
import database.tables.Unit;
import main.kernel;

/**
 * Servlet implementation class admin
 */
@WebServlet("/admin")
public class admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter pw = response.getWriter();
        pw.println("<html><head>");
        pw.println("<title>Админка</title>");
        pw.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />");
        pw.println("</head>");
        pw.println("<body>");
        String action = "def";
        if (request.getParameter("action") != null)
        	action = request.getParameter("action");
		if (action.equals("okei")) {
			pw.println("<H1>Классификатор ОКЕИ</H1>");
			pw.println("<form name=\"test1\" method=\"post\" action=\"upload\" enctype=\"multipart/form-data\">");
			pw.println("<input type=\"hidden\" name=\"action\" value=\"load_okei\">\n");
			pw.println("   <input type=\"file\" name= \"file\" value=\"Загрузить\">\n");
			pw.println("   <input type=\"submit\" value=\"Загрузить\">\n"); 
			pw.println(" </form>");
			
			List<Unit> list = kernel.getInstance().getMeasurements();
			pw.println("<table>");
			pw.println("<tr>");
			pw.println("<th>Код</th>");
			pw.println("<th>Обозначение</th>");
			pw.println("<th>Международное обозначение</th>");
			pw.println("<th>Весовой</th>");
			pw.println("</tr>");
			for (Unit elem : list) {
				pw.println("<tr>");
				pw.println("<td>" + elem.getCode() + "</td>");
				pw.println("<td>" + elem.getDesignation() + "</td>");
				pw.println("<td>" + elem.getuDesignation() + "</td>");
				pw.println("<td>" + elem.isWeight() + "</td>");
				pw.println("</tr>");
			}
			pw.println("</table>");
		} else if (action.equals("cbr")) {
			pw.println("<H1>Классификатор групп</H1>");
			pw.println("<form name=\"test1\" method=\"post\" action=\"upload\" enctype=\"multipart/form-data\">");
			pw.println("<input type=\"hidden\" name=\"action\" value=\"load_cbr\">\n");
			pw.println("   <input type=\"file\" name= \"file\" value=\"Загрузить\">\n");
			pw.println("   <input type=\"submit\" value=\"Загрузить\">\n"); 
			pw.println(" </form>");
			
			List<Group> list = kernel.getInstance().getGroups();
			pw.println("<table>");
			pw.println("<tr>");
			pw.println("<th>Группа</th>");
			pw.println("<th>Код группы</th>");
			pw.println("<th>Описание</th>");
			pw.println("</tr>");
			for (Group elem : list) {
				pw.println("<tr>");
				pw.println("<td>" + elem.getKind() + "</td>");
				pw.println("<td>" + elem.getCode() + "</td>");
				pw.println("<td>" + elem.getName() + "</td>");
				pw.println("</tr>");
			}
			pw.println("</table>");
		} else {
			pw.println("<form name=\"test1\" method=\"post\" action=\"\">");
			pw.println("<input type=\"hidden\" name=\"action\" value=\"okei\">\n");
			pw.println("   <input type=\"submit\" value=\"ОКЕИ\">\n"); 
			pw.println(" </form>");
			pw.println("<form name=\"test2\" method=\"post\" action=\"\">");
			pw.println("<input type=\"hidden\" name=\"action\" value=\"cbr\">\n");
			pw.println("   <input type=\"submit\" value=\"Группы\">\n"); 
			pw.println(" </form>");
		}
		pw.println("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter pw = response.getWriter();
        pw.println("<html><head>");
        pw.println("<title>Админка</title>");
        pw.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />");
        pw.println("</head>");
        pw.println("<body>");
        String action = "def";
        if (request.getParameter("action") != null)
        	action = request.getParameter("action");
        
        
        pw.println("</body></html>");
        
        if (action.equals("load_okei")) {
        	pw.println("<H1>Загружаем лассификатор ОКЕИ</H1>");
        	System.out.println(request.getHeader("content-disposition"));
        } else if (action.equals("load_cbr")) {
			pw.println("<H1>Загружаем классификатор групп</H1>");
		} else
			doGet(request, response);
	}

    private static String getFilename(Part part) 
    {
        for (String file : part.getHeader("content-disposition")
                               .split(";")) {
        	kernel.writeError(file);
            if (file.trim().startsWith("filename")) {
                String filename = file.substring(file.indexOf('=') + 1)
                                      .trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1)
                                              .substring(filename
                                              .lastIndexOf('\\') + 1);
            }
        }
        return null;
    }
}

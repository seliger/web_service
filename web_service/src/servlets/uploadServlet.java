package servlets;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;

import database.tables.Group;
import database.tables.Unit;
import main.kernel;

/**
 * Servlet implementation class uploadServlet
 */
@WebServlet("/upload")
@MultipartConfig
public class uploadServlet extends HttpServlet {
	private Logger log = Logger.getLogger("uploadServlet");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public uploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String description = request.getParameter("description"); // Retrieves <input type="text" name="description">
	    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
	    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
	    InputStream fileContent = filePart.getInputStream();
	    
	    response.setContentType("text/html;charset=UTF-8");
		PrintWriter pw = response.getWriter();
        pw.println("<html><head>");
        pw.println("<title>Админка</title>");
        pw.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />");
        pw.println("</head>");
        pw.println("<body>");
	    String action = "def";
        if (request.getParameter("action") != null)
        	action = request.getParameter("action");
        
	    XSSFWorkbook wbXLSX = null;
	    HSSFWorkbook wbXLS = null;
		List<String> listColumns = new ArrayList<String>();
		try {
			Iterator<Row> it = null;
			Sheet curSheet = null;
			try {
				wbXLSX = new XSSFWorkbook(fileContent);
				curSheet = wbXLSX.getSheetAt(0);
			} catch (Exception e) {
				
			}
			if (curSheet == null) {
				wbXLS = new HSSFWorkbook(fileContent);
				curSheet = wbXLS.getSheetAt(0);
			}
			it = curSheet.iterator();
			Row curRow = it.next();
			Cell curCell = curRow.getCell(0);
			
			if (action.equals("load_okei")) {
				while (it.hasNext()) {
	                curRow = it.next();
	                curCell = curRow.getCell(0);
	                Unit tmp = new Unit();
	                tmp.setCode(curCell.getStringCellValue());
	                curCell = curRow.getCell(1);
	                tmp.setName(curCell.getStringCellValue());
	                curCell = curRow.getCell(2);
	                tmp.setDesignation(curCell.getStringCellValue());
	                curCell = curRow.getCell(3);
	                tmp.setuDesignation(curCell.getStringCellValue());
	                kernel.getInstance().saveUnit(tmp);
	        	}
	        } else if (action.equals("load_cbr")) {
	        	while (it.hasNext()) {
	                curRow = it.next();
	                Cell kindCell = curRow.getCell(0);
	                Cell codeCell = curRow.getCell(1);
	                Cell nameCell = curRow.getCell(2);
	                String parent = codeCell.getStringCellValue().split("\\.")[0];
	                Group parentTmp = kernel.getInstance().loadGroupByCode(parent);
	                Group tmp = new Group(parentTmp, kindCell.getStringCellValue(), 
	                		codeCell.getStringCellValue(), nameCell.getStringCellValue());
	                kernel.getInstance().saveGroup(tmp);
	        	}
			}
			
			it = null;
			if (wbXLSX != null)
				wbXLSX.close();
			if (wbXLS != null)
				wbXLS.close();
			curSheet = null;
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage());
			for (StackTraceElement elem : e.getStackTrace()) {
				log.error("at " + elem);
			}
		}
		if (fileContent != null) {
			try {
				fileContent.close();
			} catch (IOException e) {
				log.error(e.getMessage());
			}
			fileContent = null;
		}
		listColumns.clear();
		listColumns = null;
		wbXLSX = null;
		wbXLS = null;
		pw.println("</body></html>");
	}

}

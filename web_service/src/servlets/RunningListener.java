package servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import main.kernel;

/**
 * Сервлет осуществляющий запуск ядра сервиса
 * @author den
 *
 */
public class RunningListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContextListener.super.contextInitialized(sce);
		if (!kernel.getInstance().checkConnection())
			kernel.writeError("Остуствует соединение с базой данных");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		kernel.getInstance().close();
		ServletContextListener.super.contextDestroyed(sce);
		// Попросим java почистить за нами
		System.gc();
		System.runFinalization();
	}
	
}

package database;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.jboss.logging.Logger;

import database.tables.ConstructionObject;
import database.tables.Contractor;
import database.tables.Group;
import database.tables.Nomenclature;
import database.tables.Notice;
import database.tables.Unit;
import utils.HibernateUtils;

/**
 * Основное подключение к базе данных
 * @author den
 *
 */
public class dbConnection {
	private Logger log = Logger.getLogger("DataBase");
    
	/** Текущая сессия подключения к БД */
    private SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

    /**
     * Конструктор по умолчанию
     */
    public dbConnection() {
            super();
    }

    /** Закрыть соединение **/
    public void close() {
            sessionFactory.getCurrentSession().close();
            sessionFactory.close();
    }
    
    /**
     * Проверить подключение
     * @return истина, если база доступна
     */
    public boolean checkConnection() {
    	return sessionFactory.isOpen();
    }
    
    /**
     * Сохранить группу в базу данных
     * @param inGroup группа которую необходимо сохранить
     */
    public void saveGroup(Group inGroup) {
    	Session session = sessionFactory.getCurrentSession();
        try {
                session.getTransaction().begin();
                session.saveOrUpdate(inGroup);
                session.getTransaction().commit();
        } catch (Exception e) {
                session.getTransaction().rollback();
                log.error(e.getLocalizedMessage());
                for (StackTraceElement elem :  e.getStackTrace()) {
                        log.info("at: " + elem.toString());
                }
        }
    }
    
    /**
     * Загрузить классификационную группу
     * @param inId - идентификатор группы
     * @return - группа
     */
    public Group loadGroup(long inId) {
    	Group retValue = null;
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.getTransaction().begin();
    	sessionFactory.getCurrentSession().get(Group.class, inId);
        session.getTransaction().commit();
    	
    	return retValue;
    }
    
    public void saveUnit(Unit inUnit) {
    	Session session = sessionFactory.getCurrentSession();
        try {
                session.getTransaction().begin();
                session.saveOrUpdate(inUnit);
                session.getTransaction().commit();
        } catch (Exception e) {
                session.getTransaction().rollback();
                log.error(e.getLocalizedMessage());
                for (StackTraceElement elem :  e.getStackTrace()) {
                        log.info("at: " + elem.toString());
                }
        }
    }
    
    public Unit loadUnitByCode(String inCode) {
    	Unit retValue = null;
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.getTransaction().begin();
    	Query<Unit> query = session.createQuery("FROM dict_Measurements where code = :code");
    	query.setParameter("code", inCode);
    	List<Unit> list = query.list();
    	if (!list.isEmpty())
    		retValue = list.get(0);
        session.getTransaction().commit();
    	
    	return retValue;
    }
    
	public List<Unit> getMeasurements() {
		List<Unit> retValue = new ArrayList<Unit>();

		Session session = sessionFactory.getCurrentSession();
		try {
			session.getTransaction().begin();
			retValue.addAll(session.createQuery("FROM dict_Measurements").list());
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			for (StackTraceElement elem : e.getStackTrace()) {
				log.info("at: " + elem.toString());
			}
		} finally {
			session.getTransaction().commit();
		}

		return retValue;
	}
	
	public List<Group> getGroups() {
		List<Group> retValue = new ArrayList<Group>();

		Session session = sessionFactory.getCurrentSession();
		try {
			session.getTransaction().begin();
			retValue.addAll(session.createQuery("FROM cls_Groups").list());
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			for (StackTraceElement elem : e.getStackTrace()) {
				log.info("at: " + elem.toString());
			}
		} finally {
			session.getTransaction().commit();
		}

		return retValue;
	}
	
	public Group loadGroupByCode(String inCode) {
		Group retValue = null;
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.getTransaction().begin();
    	Query<Group> query = session.createQuery("FROM cls_Groups where code = :code");
    	query.setParameter("code", inCode);
    	List<Group> list = query.list();
    	if (list.size() > 0)
    		retValue = list.get(0);
        session.getTransaction().commit();
    	
    	return retValue;
    }
	
	public Nomenclature loadNomByCode(String inExtId) {
		Nomenclature retValue = null;
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.getTransaction().begin();
    	Query<Nomenclature> query = session.createQuery("FROM dict_Nomenclatures where ext_id = :code");
    	query.setParameter("code", inExtId);
    	List<Nomenclature> list = query.list();
    	if (list.size() > 0)
    		retValue = list.get(0);
        session.getTransaction().commit();
    	
    	return retValue;
	}
	
	public void saveNom(Nomenclature inNom) {
    	Session session = sessionFactory.getCurrentSession();
        try {
                session.getTransaction().begin();
                session.saveOrUpdate(inNom);
                session.getTransaction().commit();
        } catch (Exception e) {
                session.getTransaction().rollback();
                log.error(e.getLocalizedMessage());
                for (StackTraceElement elem :  e.getStackTrace()) {
                        log.info("at: " + elem.toString());
                }
        }
    }
	
	public void saveContractor(Contractor inContractor) {
    	Session session = sessionFactory.getCurrentSession();
        try {
                session.getTransaction().begin();
                session.saveOrUpdate(inContractor);
                session.getTransaction().commit();
        } catch (Exception e) {
                session.getTransaction().rollback();
                log.error(e.getLocalizedMessage());
                for (StackTraceElement elem :  e.getStackTrace()) {
                        log.info("at: " + elem.toString());
                }
        }
    }
	
	public void saveNotice(Notice inNotice) {
    	Session session = sessionFactory.getCurrentSession();
        try {
                session.getTransaction().begin();
                session.saveOrUpdate(inNotice);
                session.getTransaction().commit();
        } catch (Exception e) {
                session.getTransaction().rollback();
                log.error(e.getLocalizedMessage());
                for (StackTraceElement elem :  e.getStackTrace()) {
                        log.info("at: " + elem.toString());
                }
        }
    }
	
	public Contractor loadContByINN(String inINN) {
		Contractor retValue = null;
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.getTransaction().begin();
    	Query<Contractor> query = session.createQuery("FROM dict_Contractors where inn = :code");
    	query.setParameter("code", inINN);
    	List<Contractor> list = query.list();
    	if (list.size() > 0)
    		retValue = list.get(0);
    	retValue.getObjects().size();
        session.getTransaction().commit();
    	
    	return retValue;
	}
	
	public ConstructionObject loadConsObjByCode(String inCode, Contractor inCont) {
		ConstructionObject retValue = null;
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.getTransaction().begin();
    	for (ConstructionObject elem : inCont.getObjects()) {
    		if (elem.getCode().equals(inCode))
    			retValue = elem;
    	}
        session.getTransaction().commit();
    	
    	return retValue;
    }
}

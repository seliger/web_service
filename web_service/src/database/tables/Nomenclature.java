package database.tables;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Номенклатура
 * @author den
 *
 */
@Entity(name = "dict_Nomenclatures")
@Table(name = "dict_Nomenclatures")
public class Nomenclature {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "name", length = 255)
	private String name;
	@Column(name = "code", length = 10)
	private String code;
	@Column(name = "subcode", length = 10)
	private String subcode;
	@Column(name = "ext_id", length = 50)
	private String ext_id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="unit_id")
	private Unit baseUnit;
	
	/**
	 * Заполнить наименование номенклатуры
	 * @param inName - наименование
	 */
	public void setName(String inName) {
		this.name = inName;
	}
	/**
	 * Заполнить код группы
	 * @param inCode - код группы
	 */
	public void setCode(String inCode) {
		this.code = inCode;
	}
	/**
	 * Заполнить код подгруппы
	 * @param inSubcode - код подгруппы
	 */
	public void setSubcode(String inSubcode) {
		this.subcode = inSubcode;
	}
	/**
	 * Заполнить внешний идентификатор
	 * @param inExt_id - внешний идентификатор
	 */
	public void setExt_id(String inExt_id) {
		this.ext_id = inExt_id;
	}
	/**
	 * Заполнить единицу измерения
	 * @param inBaseUnit код единицы измерения по ОКЕИ
	 */
	public void setBaseUnit(Unit inBaseUnit) {
		this.baseUnit = inBaseUnit;
	}
}

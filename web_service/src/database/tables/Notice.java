package database.tables;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Объявление
 * @author den
 *
 */
@Entity(name = "doc_Notices")
@Table(name = "doc_Notices")
public class Notice {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="group_id")
	private Group group;
	@Column(name = "name", length = 255)
	private String nameNom;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="measure_id")
	private Unit measure;
	@Column(name = "quantity")
	private double quantity;
	@Column(name = "cost")
	private double cost;
	@Column(name = "description", length = 1000)
	private String description;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="constobj_id")
	private ConstructionObject constObj;
	@Column(name = "substandard")
	private boolean substandard;
	@Column(name = "shelflife")
	private Date shelfLife;
	@Column(name = "urgentsale")
	private boolean urgentSale;
	/**
	 * Заполнить группу
	 * @param group - группа
	 */
	public void setGroup(Group inGroup) {
		this.group = inGroup;
	}
	public void setNameNom(String inNameNom) {
		this.nameNom = inNameNom;
	}
	public void setMeasure(Unit inMeasure) {
		this.measure = inMeasure;
	}
	public void setQuantity(double inQuantity) {
		this.quantity = inQuantity;
	}
	public void setCost(double inCost) {
		this.cost = inCost;
	}
	public void setDescription(String inDescription) {
		this.description = inDescription;
	}
	public void setConstObj(ConstructionObject inConstObj) {
		this.constObj = inConstObj;
	}
	public void setSubstandard(boolean inSubstandard) {
		this.substandard = inSubstandard;
	}
	public void setShelfLife(Date inShelfLife) {
		this.shelfLife = inShelfLife;
	}
	public void setUrgentSale(boolean inUrgentSale) {
		this.urgentSale = inUrgentSale;
	}
}

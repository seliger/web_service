package database.tables;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Контрагенты
 * @author den
 *
 */
@Entity(name = "dict_Contractors")
@Table(name = "dict_Contractors")
public class Contractor {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "fio", length = 255)
	private String fio;
	@Column(name = "name", length = 255)
	private String name;
	@Column(name = "inn", length = 12)
	private String inn;
	@Column(name = "kpp", length = 9)
	private String kpp;
	@Column(name = "owndelivery")
	private boolean ownDelivery;
	@Column(name = "legaladdress", length = 150)
	private String legalAddress;
	@Column(name = "actualaddress", length = 150)
	private String actualAddress;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ConstructionObject> consObjects = new ArrayList<ConstructionObject>();
	@Column(name = "mail", length = 150)
	private String mail;
	@Column(name = "phone", length = 50)
	private String phone;
	@Column(name = "checkingaccount", length = 20)
	private String checkingAccount;
	@Column(name = "costdelivery")
	private double costDelivery;
	
	public long getId() {
		return this.id;
	}
	
    public List<ConstructionObject> getObjects() {
        return this.consObjects;
    }
}

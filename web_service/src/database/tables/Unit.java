package database.tables;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "dict_Measurements")
@Table(name = "dict_Measurements")
public class Unit {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "code", length = 5)
	private String code;
	@Column(name = "name", length = 100)
	private String name;
	@Column(name = "designation", length = 10)
	private String designation;
	@Column(name = "uDesignation", length = 10)
	private String uDesignation;
	@Column(name = "weight")
	private boolean weight;
	
	public String getCode() {
		return code;
	}
	public String getDesignation() {
		return designation;
	}
	public String getName() {
		return name;
	}
	public String getuDesignation() {
		return uDesignation;
	}
	public boolean isWeight() {
		return weight;
	}
	public void setCode(String inCode) {
		this.code = inCode;
	}
	public void setDesignation(String inDesignation) {
		this.designation = inDesignation;
	}
	public void setName(String inName) {
		this.name = inName;
	}
	public void setuDesignation(String inUDesignation) {
		this.uDesignation = inUDesignation;
	}
	public void setWeight(boolean inWeight) {
		this.weight = inWeight;
	}
}

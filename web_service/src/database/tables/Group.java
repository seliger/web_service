package database.tables;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Классификационная группа
 * @author den
 */
@Entity(name = "cls_Groups")
@Table(name = "cls_Groups")
public class Group {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="parent_id")
	private Group parent;
	@Column(name = "kind", length = 10)
	private String kind;
	@Column(name = "code", length = 10)
	private String code;
	@Column(name = "name", length = 255)
	private String name;
	
	public Group() {
		super();
		this.parent = null;
		this.kind = "";
		this.code = "";
		this.name = "";
	}
	
	public Group(Group parent, String kind, String code, String name) {
		super();
		this.parent = parent;
		this.kind = kind;
		this.code = code;
		this.name = name;
	}

	public String getKind() {
		return kind;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

package database.tables;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "dict_consObjects")
@Table(name = "dict_consObjects")
public class ConstructionObject {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "code", length = 50)
	private String code;
	@Column(name = "actualaddress", length = 150)
	private String actualAddress;
	/*private Contractor contractor;
	 
    @ManyToOne
    @JoinColumn(name = "contractor_id")
    public Contractor getContractor() {
        return this.contractor;
    }*/
	public String getCode() {
		return code;
	}
}

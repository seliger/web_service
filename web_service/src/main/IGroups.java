package main;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Интерфейс для классификационных групп
 * @author den
 *
 */
@WebService
public interface IGroups {
	@WebMethod
	public void addGroup(@WebParam(name = "parent_id") long inParent, 
			@WebParam(name = "kind") String inKind,
			@WebParam(name = "code") String inCode,
			@WebParam(name = "name") String inName);
}

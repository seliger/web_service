package main;

import org.jboss.logging.Logger;

import database.dbConnection;

/**
 * Ядро системы
 * @author den
 *
 */
public class kernel {
	private static Logger log = Logger.getLogger("kernel");
	/** Переменная текущего подключения **/
	private static dbConnection instance = null;
	/**
	 * Запросить текущее подключение 
	 * @return текущее подключение к Базе данных
	 */
	public static dbConnection getInstance()
    {
        if (instance == null)
            instance = new dbConnection();
        return instance;
    }
	
	public static void writeError(String inText) {
		log.info(inText);
	}
}

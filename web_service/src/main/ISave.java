package main;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import loadData.lineData;

@WebService
public interface ISave {
	public void Save(@WebParam(name = "rows", targetNamespace = "http://main/") List<lineData> inList);
}

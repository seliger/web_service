package main;

import java.util.List;

import javax.jws.WebService;
import javax.xml.ws.http.HTTPException;

import org.jboss.logging.Logger;

import database.tables.Notice;
import loadData.lineData;

@WebService
public class loadData implements ISave {
	private Logger log = Logger.getLogger("loadData");

	@Override
	public void Save(List<lineData> inList) {
		try {
			for (lineData elem : inList) {
				// Сохраним или обновим номенклатуру
				kernel.getInstance().saveNom(elem.getNomenclature());
				Notice tmpNotice = new Notice();
				tmpNotice.setGroup(elem.getGroup());
				tmpNotice.setCost(elem.cost / 100);
				tmpNotice.setDescription(elem.description);
				tmpNotice.setMeasure(elem.getMeasurement());
				tmpNotice.setNameNom(elem.nameNom);
				tmpNotice.setQuantity(elem.quantity);
				tmpNotice.setShelfLife(elem.getShelfLifeDate());
				tmpNotice.setSubstandard(elem.substandard);
				tmpNotice.setUrgentSale(elem.urgentSale);
				tmpNotice.setConstObj(kernel.getInstance().loadConsObjByCode(elem.codeOB, elem.getContractor()));
				kernel.getInstance().saveNotice(tmpNotice);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			for (StackTraceElement elem :  e.getStackTrace()) {
                log.info("at: " + elem.toString());
        }
			throw new HTTPException(500);
		}
	}

}

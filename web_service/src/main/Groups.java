package main;

import javax.jws.WebService;

import database.tables.Group;

/**
 * Сервис для классификационных групп
 * @author den
 *
 */
@WebService
public class Groups implements IGroups {

	@Override
	public void addGroup(long inParent, String inKind, String inCode, String inName) {
		Group parent = kernel.getInstance().loadGroup(inParent);
		Group tmpGroup = new Group(parent, inKind, inCode, inName);
		kernel.getInstance().saveGroup(tmpGroup);
	}

}

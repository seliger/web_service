package utils;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtils {

    private static final SessionFactory sessionFactory = buildSessionFactory();
    private static ServiceRegistry serviceRegistry;

    private static SessionFactory buildSessionFactory() {
    	Configuration configuration = null;
    	try {
	        configuration = new Configuration();
	        configuration.configure();
	        Properties properties = configuration.getProperties();
	        // Создание базы данных
	        //properties.put("hibernate.hbm2ddl.auto", "create");
	        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();
    	} catch (Exception e) {
        	
        }
        return configuration.buildSessionFactory();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
